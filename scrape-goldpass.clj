#_(defdeps
    [[org.clojure/data.csv "0.1.2"]
     [org.seleniumhq.selenium/selenium-java "2.53.1"]])


;; Instructions (alternatively, follow README.md):
;;   1) Download chromeDriver.exe and set the constants
;;
;;   2) Install jdk >= 1.6, leiningen >= 2.0, and Google Chrome
;;
;;   3) Install lein-oneoff by adding to '~/.lein/profiles.clj'
;;      https://github.com/mtyaka/lein-oneoff#installation
;;
;;   4) Run script like 'lein oneoff scrape-goldpass.clj'
;;      https://github.com/mtyaka/lein-oneoff#exec


;; Constants for user to change (see #1 above)
(def username "x500 username here")
(def password "x500 password here")

(def chromedriver-path "/home/tim/projects/goldpass-scraper/chromedriver")
(def chrome-binary "/usr/bin/google-chrome-beta")
(def links-output "links-output.txt")
(def jobs-output "jobs-output.csv")
(def sleep-duration 300)
(def labels ["URL"                        ;; Labels written at top of csv output
             "Job title"
             "Employer"
             ;; Position information
             "Linked schedule"
             "Number of openings"
             "Hours per week"
             "Wage/salary"
             "Job paid"
             "Employment start date"
             "Employment end date"
             "Job description"
             "Qualifications"
             "Application method"
             "Application method details"
             "On-line application address"
             ;; Posting information
             "Job location"
             "Job category"
             "Position type"
             "Minimun GPA"
             "Work authorizations considered for this position"
             "Graduation start"
             "Graduation end"
             "Classification"
             "Degree(s) seeking or attained"
             "Majors"
             "Post date"
             "Expiration date"])


;; Rest of script should work without change.
(require '[clojure.java.io :as io]
         '[clojure.data.csv :as csv])

(import org.openqa.selenium.WebDriver
        org.openqa.selenium.chrome.ChromeDriver
        org.openqa.selenium.chrome.ChromeOptions
        org.openqa.selenium.JavascriptExecutor
        org.openqa.selenium.NoSuchElementException
        org.openqa.selenium.StaleElementReferenceException
        java.io.File)

(def chrome-options (doto (new ChromeOptions)
                      (.setBinary chrome-binary)))

(defn has-next-page [driver]
  (let [groups (re-find #"Viewing records \d+ - (\d+) of (\d+)"
               (.getText (.findElementByCssSelector driver "#info span")))]
    (dorun groups)
    (< (Integer/parseInt (second groups))
       (Integer/parseInt (nth groups 2)))))


(defn wait-to-load [driver]
  (if (or (try (not
            (do (while (try
                         (do (.contains (.getText (.findElementByCssSelector driver "#info span")) "Viewing records ")
                             false)
                         (catch StaleElementReferenceException e true)))
              true))
                (catch NoSuchElementException e true))
           (= (.getCssValue (.findElementById driver "loadSearch") "display") "block"))
    (do (Thread/sleep sleep-duration)
        (wait-to-load driver))))


(defn next-page [driver]
  (.executeScript (cast  JavascriptExecutor driver)
                  "GoNext(); console.log( 'Executed GoNext()');"
                  (make-array Object 0))
  (wait-to-load driver))


(defn jump-to-page [driver page]
  (.executeScript (cast  JavascriptExecutor driver)
                  (str "GoPage(" page "); console.log( 'Executed GoNext()');")
                  (make-array Object 0))
  (wait-to-load driver))


(defn parse-links [driver]
  (let [job-links (map (fn [web-element] (.getAttribute web-element "href"))
                       (.findElementsByCssSelector driver ".title a"))]
    (dorun job-links)                                     ; force evaluation of lazy-seq for side-effects
    (doseq [job-link job-links]
      (with-open [wrtr (io/writer links-output :append true)]
        (.write wrtr (str job-link "\n"))))))


(defn login [driver]
  ; (.get driver "https://goldpass.umn.edu")

  ;; Click 'Students & Alumni: LOGIN'
  ; (.click (.findElementByCssSelector driver ".Menu area"))
  (.get driver "https://www.myinterfase.com/goldpass/student/secure/home.aspx")

  ;; Login on the user's behalf
  (.sendKeys (.findElementById driver "username") (into-array [username]))
  (.sendKeys (.findElementById driver "password") (into-array [password]))
  (.click (.findElementByClassName driver "idp3_form-submit")))


(defn write-links-to-file []
  (System/setProperty "webdriver.chrome.driver" chromedriver-path)
  (with-open [driver (new ChromeDriver chrome-options)]
    (login driver)

    ;; Navigate to search criteria
    (.get driver "https://www.myinterfase.com/goldpass/Search/Job/?id=1")
    ; (.click (first (.findElementsByPartialLinkText driver "Job/Internship Search")))
    ; (.click (second (.findElementsByPartialLinkText driver "Job/Internship Search")))

    ;; Perform search and wait for the results
    (.click (.findElementByClassName driver "btn-search"))
    (wait-to-load driver)

    ;; TODO: determine current page from number of links already written
    ; (jump-to-page driver 41)                   ;; Skip already processed pages

    (println "Saving links to each job. This will take a long time.")

    (while (has-next-page driver)
      (parse-links driver)
      (next-page driver))
    (parse-links driver)                         ;; Parse last page

    (println "Finished writing links.")))


(defn parse-links-to-csv []
  (System/setProperty "webdriver.chrome.driver" chromedriver-path)
  (with-open [driver (new ChromeDriver chrome-options)]
    (login driver)

    (with-open [rdr (clojure.java.io/reader links-output)]
      (with-open [wrtr (io/writer jobs-output)]
        (println "Saving job information to csv. This will take a long time.")
        (csv/write-csv wrtr [labels])
        ;; TODO: skip current line if its already in the csv file
        (doseq [line (line-seq rdr)]    ;; We could parallelize this using multiple webdrivers...
          (.get driver line)
          (Thread/sleep sleep-duration) ;; But we're being nice to Goldpass' servers

          ;; Write job details to CSV file, avoiding selenium's stale references
          ;; by looping until exceptions disappear and doall
          (while (try (do
                        (csv/write-csv wrtr (doall [(cons line
                                                            (doall (map (fn [web-element] (.getText web-element))
                                                                        (concat (.findElementsByCssSelector driver "#pageBody .detail-page-header")
                                                                                (.findElementsByCssSelector driver "#Position_Information .item-value")
                                                                                (.findElementsByCssSelector driver "#Posting_Information .item-value")))))]))
                        false)
                      (catch StaleElementReferenceException e (do (Thread/sleep sleep-duration) true))))))))
  (println "Finished writing jobs to csv file."))


(write-links-to-file)
(parse-links-to-csv)
(println (str "Opening " jobs-output "..."))
(.open (java.awt.Desktop/getDesktop) (new File jobs-output))
(println "Execution finished.")
