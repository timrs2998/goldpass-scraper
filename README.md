# Goldpass Scraper

A Clojure app that saves the links for each job posted on
[Goldpass](http://goldpass.umn.edu/) with the default search criteria, then
visits each link to save each each job as a record in a csv file intended for
Excel with the en-US locale.

## Setup

[Download ChromeDriver](https://sites.google.com/a/chromium.org/chromedriver/)
and extract the zip to the project directory. Set its path in
'scrape-goldpass.clj' as the chromedriver-path constant. You may need the latest
ChromeDriver (2.35) to support your Chrome installation.

Install the JDK version 1.6 or later as well as Google Chrome.

[Install Leiningen](http://leiningen.org/#install) 2.0 or later. On Windows, you
may need [Wget](http://users.ugent.be/~bpuype/wget/). With 'lein.bat' and
'wget.exe' in the same directory, you can hold shift, right click, and choose
'Open Command Window Here'. Then, you can run `lein self-install` to complete
the installation.

[Install lein-oneoff](https://github.com/mtyaka/lein-oneoff#installation)
by placing `{:user {:plugins [[lein-oneoff "0.3.1"]]}}` in
'~/.lein/profiles.clj'.

## Running

In the project directory, run `lein oneoff scrape-goldpass.clj`. Wait several
hours and hopefully you will have a csv file containing every job on Goldpass.
